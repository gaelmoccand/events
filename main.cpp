#include <vector>
#include <cinttypes>
#include <iostream>
#include <algorithm>
#include <functional>
#include <future>

namespace Evt {

struct Event
{
    uint64_t id = 0;
    uint32_t timestamp = 0;
    Event(uint64_t id, uint32_t timestamp):id(id),timestamp(timestamp){}
};

using Events = std::vector<Event>;

struct Forked
{
    Events ev1;
    Events ev2;
};

using Predicate = std::function<bool(Event&)>;
using Trans = std::function<Event(Event&)>;

class Filter
{
  public:
    Filter() = delete;
    Filter(Filter& ) = delete;
    Filter& operator=(Filter& ) = delete; 
    Filter(Predicate pred):m_pred(pred){};
    Filter(Trans trans):m_trans(trans){};

    void Apply(Events&& input);
    Events Get();
    Forked Fork(Events&& input);
    
  private:
    std::optional<Predicate> m_pred = std::nullopt;
    std::optional<Trans> m_trans = std::nullopt;
    std::future<Events>  m_output;
};

Forked Filter::Fork(Events&& input)
{
    Events copied = input;
    Forked forked;
    forked.ev1 = input;
    forked.ev2 = copied;
    return forked;
}

void Filter::Apply(Events&& input)	
{
    if (m_pred)
    {
        m_output = std::async(std::launch::async, [&](Events&& ev)
        {
            auto it = std::remove_if(ev.begin(), ev.end(), *m_pred);
            ev.erase(it, ev.end());
            return (ev);
        }, std::move(input));
    }
    else if (m_trans)
    {
        m_output = std::async(std::launch::async, [&](Events&& ev)
        {
            auto it = std::transform(ev.begin(), ev.end(),ev.begin(), *m_trans);
            return (ev);
        }, std::move(input));
    }
}

Events Filter::Get(){
    if (m_pred || m_trans)
    {
        return m_output.get();
    }
    else {
        return {};
    }
}

} // end namespace Evt

int main() {

    auto rmIdLess12 = [](const auto & elem) { return elem.id < 12;};
    Evt::Filter filter1(rmIdLess12);
    auto add1Id = [](auto & elem) { elem.id++; return elem;};
    Evt::Filter filter2(add1Id);
    auto add2Id = [](auto &  elem) { elem.id+=2; return elem;};
    Evt::Filter filter3(add2Id);
    auto resetTime = [](auto &  elem) { elem.timestamp=0; return elem;};
    Evt::Filter filter4(resetTime);

    filter1.Apply({{10,1},{11,2},{12,3},{13,4},{14,5}});
    filter2.Apply(filter1.Get());
    filter3.Apply(filter2.Get());
    auto [outFiltered, outCopied] = filter3.Fork(filter3.Get());
    filter4.Apply(std::move(outFiltered)); // might overload Apply method with Events&
    outCopied.emplace_back(20,11);

    std::cout << "The forked events output: \n";
    for (const auto& ev : outCopied)
    {
        std::cout <<"id_: " << ev.id << " timestamp_: "<< ev.timestamp << "\n";
    }

    std::cout << "The filetered events output after timestamp reset: \n";
    for (const auto& ev : filter4.Get())
    {
        std::cout <<"id_: " << ev.id << " timestamp_: "<< ev.timestamp << "\n";
    }
}
